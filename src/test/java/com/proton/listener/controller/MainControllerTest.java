package com.proton.listener.controller;

import com.proton.listener.config.AppConfigTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
@AutoConfigureMockMvc
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetIndexPage() throws Exception {
        this.mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andDo(print());
    }


    @Test
    public void testGetContentWhenBothUrlAndBothWordIsNull() throws Exception {
        this.mockMvc.perform(post("/")
                .param("urlFirst", "")
                .param("wordFirst", "")
                .param("urlSecond", "")
                .param("wordSecond", "")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGetContentWhenOnlyFirstUrlAndFirsWordIsPresent() throws Exception {
        this.mockMvc.perform(post("/")
                .param("urlFirst", "https://stackoverflow.com")
                .param("wordFirst", "cod")
                .param("urlSecond", "")
                .param("wordSecond", "")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGetContentWhenOnlyFirstUrlIsPresent() throws Exception {
        this.mockMvc.perform(post("/")
                .param("urlFirst", "https://stackoverflow.com")
                .param("wordFirst", "")
                .param("urlSecond", "")
                .param("wordSecond", "")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }


    @Test
    public void testGetContentWhenOnlySecondUrlAndFirsWordIsPresent() throws Exception {
        this.mockMvc.perform(post("/")
                .param("urlFirst", "https://stackoverflow.com")
                .param("wordFirst", "cod")
                .param("urlSecond", "")
                .param("wordSecond", "")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGetContentWhenOnlySecondUrlIsPresent() throws Exception {
        this.mockMvc.perform(post("/")
                .param("urlFirst", "")
                .param("wordFirst", "")
                .param("urlSecond", "https://stackoverflow.com")
                .param("wordSecond", "")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }


    @Test
    public void testgetContentWhenBothUrlAndBothWorldIsPresent() throws Exception {
        this.mockMvc.perform(post("/")
                .param("urlFirst", "https://stackoverflow.com")
                .param("wordFirst", "cod")
                .param("urlSecond", "https://stackoverflow.com")
                .param("wordSecond", "cod")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }


    @Test
    public void testPostRequestWhenUrlPathNotExist() throws Exception {
        this.mockMvc.perform(post("/test"))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }

    @Test
    public void testGetRequestWhenPathDoesNotExist() throws Exception {
        this.mockMvc.perform(get("/test"))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }

}
