package com.proton.listener.servise;

import com.proton.listener.config.AppConfigTest;
import com.proton.listener.model.Content;
import com.proton.listener.repository.ContentRepository;
import com.proton.listener.service.ContentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
public class ContentServiceTest {

    @Autowired
    private ContentService contentService;

    @MockBean
    private ContentRepository contentRepository;

    private Content content;

    @Before
    public void setUp(){
        content = new Content("http://localhost:8080/", 200, 837l, 2345l, "test", true);
    }

    @Test
    public void testGetAllResultWhenResultTrue(){
        List<Content> results = new ArrayList<>();
        results.add(content);
        when(contentRepository.findAll()).thenReturn(results);
        assertThat(contentService.getContents().size(), is(1));
    }

    @Test
    public void testGetAllResultWhenResultFalse(){
        List<Content> results = new ArrayList<>();
        results.add(content);
        when(contentRepository.findAll()).thenReturn(results);
        assertThat(contentService.getContents().size(), is(not(4)));
    }

    @Test
    public void testGetAllResultWhenResultIsEmpty(){
        List<Content> results = new ArrayList<>();
        when(contentRepository.findAll()).thenReturn(results);
        assertThat(contentService.getContents(), is(Collections.emptyList()));
    }

    @Test
    public void testSaveContentWhenResultTrue(){
        when(contentRepository.save(any())).thenReturn(content);
        assertThat(contentService.saveContent(content), is(content));
    }

    @Test
    public void testSaveContentWhenResultFalse(){
        when(contentRepository.save(any())).thenReturn(content);
        assertThat(contentService.saveContent(content), is(not(4)));
    }

    @Test(expected = NullPointerException.class)
    public void testSaveContentWhenResultExseption(){
        when(contentRepository.save(any())).thenThrow(NullPointerException.class);
        contentService.saveContent(content);
    }
}
