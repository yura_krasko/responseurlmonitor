package com.proton.listener.repository;

import com.proton.listener.config.AppConfigTest;
import com.proton.listener.model.Content;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
@DataJpaTest
public class ContentRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ContentRepository contentRepository;

    private Content newContent;

    @Before
    public void setUp(){
        newContent = new Content("http://localhost:8080/", 200, 837l, 2345l, "test", true);
    }

    @Test
    public void testFindAllWhenResultTrue (){
      contentRepository.save(newContent);
      assertThat(contentRepository.findAll().size(), is(1));
    }

    @Test
    public void testFindAllWhenResultFalse (){
        contentRepository.save(newContent);
        assertThat(contentRepository.findAll().size(), is(not(5)));
    }

    @Test
    public void testSaveWhenResultTrue (){
       assertThat(contentRepository.save(newContent), is(newContent));
    }

    @Test
    public void testSaveWhenResultFalse (){
        assertThat(contentRepository.save(newContent), is(not(new Content())));
    }
}
