package com.proton.listener.controller;

import com.proton.listener.model.Content;
import com.proton.listener.service.ContentService;
import com.proton.listener.service.RestTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Controller
@RequestMapping("/")
public class MainController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private RestTemplateService restTemplateResult;

    @Autowired
    private ContentService contentService;

    /**
     * Open page index.ftl by path http://localhost:8080/ when GET request
     *
     * @return page of name index.ftl
     */
    @GetMapping
    public String getIndexPage(){
        return "index";
    }

    /**
     * Open page index.ftl by path http://localhost:8080/ when POST request
     *
     * @param urlFirst url of first input.
     * @param wordFirst first word.
     * @param urlSecond url of second input.
     * @param wordSecond second word.
     * @param model
     * @return page of name index.ftl
     */
    @PostMapping
    public String getContent( @RequestParam("urlFirst") String urlFirst,
                              @RequestParam("wordFirst") String wordFirst,
                              @RequestParam("urlSecond") String urlSecond,
                              @RequestParam("wordSecond") String wordSecond,
                              final Model model) {
        try {
            CompletableFuture<Content> contentFirstUrl = restTemplateResult.getContentByUrl(urlFirst, wordFirst);
            CompletableFuture<Content> contentSecondUrl = restTemplateResult.getContentByUrl(urlSecond, wordSecond);
            contentService.saveContent(contentFirstUrl.get());
            contentService.saveContent(contentSecondUrl.get());
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.debug("Extracting data failed! Reason: : "+ e.getMessage());
        }
        model.addAttribute("contents", contentService.getContents());
        return "index";
    }
}
