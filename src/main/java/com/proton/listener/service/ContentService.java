package com.proton.listener.service;

import com.proton.listener.model.Content;

import java.util.List;

public interface ContentService<T> {

    /**
     * Method for getting all object from database.
     *
     * @return list of objects from database or empty list otherwise
     */
    List<T> getContents();

    /**
     * Method for creating new object into database.
     *
     * @param entity object for inserting.
     * @return object.
     */
     T saveContent(Content entity);
}
