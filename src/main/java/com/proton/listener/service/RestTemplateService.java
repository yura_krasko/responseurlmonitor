package com.proton.listener.service;

import java.util.concurrent.CompletableFuture;

public interface RestTemplateService<T> {

    /**
     * Returns object after http request in a separate thread.
     *
     * @param url url for request and word for find in response body.
     * @return object in a separate thread.
     */
    CompletableFuture<T> getContentByUrl(String url, String word) ;
}
