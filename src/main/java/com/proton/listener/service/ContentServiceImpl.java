package com.proton.listener.service;

import com.proton.listener.model.Content;
import com.proton.listener.repository.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentServiceImpl implements ContentService {

    @Autowired
    private ContentRepository contentRepository;

    /**
     * Implementing method for get all contents from the  database.     *
     * @return list of content.
     */
    @Override
    public List<Content> getContents() {
        return contentRepository.findAll();
    }

    /**
     * Implementing method for save content into database.
     *
     * @param content content for inserting.
     * @return content.
     */
    @Override
    public Content saveContent(Content content) {
        return contentRepository.save(content);
    }

}

