package com.proton.listener.service;

import com.proton.listener.handler.RestTemplateResponseErrorHandler;
import com.proton.listener.model.Content;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class RestTemplateServiceImpl implements RestTemplateService {

    private RestTemplate restTemplate;

    @Autowired
    private HttpHeaders headers;

    @Autowired
    public RestTemplateServiceImpl(RestTemplateBuilder restTemplateBuilder) {
       restTemplate = restTemplateBuilder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }

    /**
     * Call the URL and then convert the response to Content object.
     * The method will be started in a separate thread.
     *
     * @param url url for request and word for find in response body.
     * @return content.
     */
    @Override
    @Async
    public CompletableFuture<Content> getContentByUrl(final String url, final String word) {
        Content content;
        final Long startTime = System.currentTimeMillis();
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        Long elapsedTime = System.currentTimeMillis() - startTime;
        headers = response.getHeaders();
        if (response.getBody().contains(word) && !word.isEmpty()) {
            content = new Content(url, response.getStatusCodeValue(),
                    elapsedTime, headers.getContentLength(), word, true);
        } else {
            content = new Content(url, response.getStatusCodeValue(),
                    elapsedTime, headers.getContentLength(), word,false);
        }
        return CompletableFuture.completedFuture(content);
    }
}
