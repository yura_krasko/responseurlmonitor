package com.proton.listener.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String url;
    private Integer httpStatus;
    private Long responseTime;
    private Long contentLength;
    private String requestWord;
    private Boolean present;

    public Content() {
    }

    public Content(String url, Integer httpStatus, Long responseTime, Long contentLength, String requestWord, Boolean present) {
        this.url = url;
        this.httpStatus = httpStatus;
        this.responseTime = responseTime;
        this.contentLength = contentLength;
        this.requestWord = requestWord;
        this.present = present;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public Long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Long responseTime) {
        this.responseTime = responseTime;
    }

    public Long getContentLength() {
        return contentLength;
    }

    public void setContentLength(Long contentLength) {
        this.contentLength = contentLength;
    }

    public String getRequestWord() {
        return requestWord;
    }

    public void setRequestWord(String requestWord) {
        this.requestWord = requestWord;
    }

    public Boolean getPresent() {
        return present;
    }

    public void setPresent(Boolean present) {
        this.present = present;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Content content = (Content) o;
        return Objects.equals(id, content.id) &&
                Objects.equals(url, content.url) &&
                Objects.equals(httpStatus, content.httpStatus) &&
                Objects.equals(responseTime, content.responseTime) &&
                Objects.equals(contentLength, content.contentLength) &&
                Objects.equals(requestWord, content.requestWord) &&
                Objects.equals(present, content.present);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, url, httpStatus, responseTime, contentLength, requestWord, present);
    }
}
