package com.proton.listener.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestTemplateResponseErrorHandler.class);

    /**
     * Given httpResponse client(4xx) and server(5xx) errors.
     * @param httpResponse the response to inspect.
     * @return true when response has an error or false otherwise
     */
    @Override
    public boolean hasError(ClientHttpResponse httpResponse) {
        try {
            return (httpResponse.getStatusCode().series() == CLIENT_ERROR
                    || httpResponse.getStatusCode().series() == SERVER_ERROR
            );
        } catch (IOException e) {
            LOGGER.debug("Exception: "+ e.getMessage());
            return false;
        }
    }

    /**
     * Handle the client(4xx) and server(5xx) errors in the response.
     * @param httpResponse the response to inspect.
     */
    @Override
    public void handleError(ClientHttpResponse httpResponse) {
        try {
            if (httpResponse.getStatusCode()
                    .series() == HttpStatus.Series.SERVER_ERROR) {
            } else if (httpResponse.getStatusCode()
                    .series() == HttpStatus.Series.CLIENT_ERROR) {
            }
        } catch (IOException e) {
            LOGGER.debug("Exception: "+ e.getMessage());
        }
    }
}
