package com.proton.listener.repository;

import com.proton.listener.model.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for CRUD operations.
 */

@Repository
public interface ContentRepository extends JpaRepository<Content, Long> {
}
