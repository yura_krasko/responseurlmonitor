<#--Color for the Response Time cell.
    If Response Time less 500 then color cell white, otherwise color cell red-->
<#function timeColor content>
    <#if (content.responseTime <= 500)>
        <#return "white" />
    <#else>
        <#return "red" />
    </#if>
</#function>

<#--Color for the Content-Length cell.
    If Content-Length less or equals 1 then color cell white, otherwise color cell red-->
<#function lengthColor content>
    <#if (content.contentLength >= 1)>
        <#return "white" />
    <#else>
        <#return "red" />
    </#if>
</#function>


<#--Color for the Request Word cell.
     if request word is not empty and present is true - color of cell green
     otherwise color of cell red. -->
<#function requestWordColor content>
    <#if content.present!false >
        <#return "green" />
    <#else>
        <#return "red" />
    </#if>
</#function>