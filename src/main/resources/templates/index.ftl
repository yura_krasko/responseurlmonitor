<#import "parts/common.ftl" as common>
<#import "parts/fuctions.ftl" as function>
<@common.page>
<form action="/" method="post">
    <div align="center">
        <input type="text" name="urlFirst" size="70"
               placeholder="Enter request URL">
        <input type="text" name="wordFirst" size="15"
               placeholder="Enter specified word">
    </div>
    <div align="center">
        <input type="text" name="urlSecond" size="70"
               placeholder="Enter request URL">
        <input type="text" name="wordSecond" size="15"
               placeholder="Enter specified word">
    </div>
    <div align="center">
        <input type="submit" value="Submit" size="30" class="btn btn-primary  btn-lg mx-auto my-lg-4 " >
    </div>
</form>

<#if contents??>
<table class="table table-bordered" align="center">
    <thead class="bg-primary text-white" align="center">
        <th scope="col">URL</th>
        <th scope="col">HTTP Status</th>
        <th scope="col">Response Time</th>
        <th scope="col">Content-Length</th>
        <th scope="col">Word</th>
    </thead>
    <tbody>
    <#list contents as content>
     <tr scope="row" align="center">
         <td>${content.url}</td>
         <td>${content.httpStatus}</td>
         <td bgcolor=${function.timeColor(content)}>${content.responseTime}ms</td>
         <td bgcolor=${function.lengthColor(content)}>${content.contentLength}</td>
         <td bgcolor=${function.requestWordColor(content)}>${content.requestWord}</td>
     </tr>
     </#list>
    </tbody>
</table>
</#if>
</@common.page>