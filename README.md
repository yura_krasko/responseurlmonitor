# Spring based web application to monitor URL responses. 

### Technologies
* Java 10
* Spring BOOT
* Gradle
* H2 database
* Spring MVC
* Spring JPA
* FreeMarker


### Link for clone project 
```shell
$ git clone https://bitbucket.org/yura_krasko/responseurlmonitor
```

## Task To Be Done 
When opening the web application, the user must center URL path that must be periodically listened & record:

+ response time;
+ response code;
+ response length;
+ check if response text contains specified word;

Highlight rows with responses that don�t satisfy the criteria (for example, response time is too high). The ability to listen to a few URLs at the same time must be implemented

#### Criterias

+ when response time less 500 ms,  color of cell white, otherwise color of cell red;
+ when content-length more 1,  color of cell white, otherwise color cell red;
+ when response text contains specified word, color of cell green, else red;